using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterModel
{
    private Vector2Int _initialPosition;
    public Vector2Int CurrentPosition {  get; private set; }

    public CharacterModel(Vector2Int initialPosition)
    {
        _initialPosition = initialPosition;
        CurrentPosition = initialPosition;
    }

    public void Setup(Vector2Int initialPosition)
    {
        _initialPosition = initialPosition;
    }

    public void MoveToPosition(Vector2Int destination)
    {
        CurrentPosition = destination;
    }

    public void Reset()
    {
        CurrentPosition = _initialPosition;
    }
}
