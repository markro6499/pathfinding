using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class MapModel
{
    public MapTemplate Config {  get; private set; }

    private TileModel[,] _tiles;
    private List<ObstacleModel> _obstacles = new List<ObstacleModel>();

    private int _x;
    private int _y;
    public int X => _x;
    public int Y => _y;

    public TileModel[,] Tiles => _tiles;
    public List<ObstacleModel> Obstacles => _obstacles;

    int lastObstacleId = 0;

    public int MapSize
    {
        get
        {
            return _x * _y;
        }
    }


    public MapModel(MapTemplate template)
    {
        Config = template;

        _x = template.MapSize.x;
        _y = template.MapSize.y;

        _tiles = new TileModel[_x, _y];

        for (int i =  0; i < template.MapSize.x; i++)
        {
            for (int j = 0; j < template.MapSize.y; j++)
            {
                Node node = new Node(true, i, j);
                _tiles[i,j] = new TileModel(i,j, node);
            }
        }

        for (int i = 0; i < template.ObstaclesPositions.Length; i++)
        {
            if (template.ObstaclesPositions[i].x > 0 && template.ObstaclesPositions[i].y > 0)
            {
                if (template.ObstaclesPositions[i].x < _x && template.ObstaclesPositions[i].y < _y)
                {
                    if (_tiles[template.ObstaclesPositions[i].x, template.ObstaclesPositions[i].y].IsOccupied == false)
                        AddObstacle(template.ObstaclesPositions[i]);
                }

            }
        }
    }

    public ObstacleModel AddObstacle(Vector2Int position)
    {
        _tiles[position.x, position.y].SetOccupant(lastObstacleId);
        ObstacleModel obstacleModel = new ObstacleModel(lastObstacleId, new Vector2Int(position.x, position.y));
        _obstacles.Add(obstacleModel);
        lastObstacleId++;

        return obstacleModel;
    }

    public void RemoveObstacle(int id)
    {
        for (int i = _obstacles.Count; i-- > 0;)
        {
            if (_obstacles[i].Id == id)
            {
                _tiles[_obstacles[i].Position.x, _obstacles[i].Position.y].Clear();
                _obstacles.RemoveAt(i);
                return;
            }
        }
    }

    public List<Node> GetNeighbours(Node node)
    {
        List<Node> neighbors = new List<Node>();

        Vector2Int[] directions = new Vector2Int[]
            {
                new Vector2Int(0, 1), // Up
                new Vector2Int(0, -1), // Down
                new Vector2Int(-1, 0), // Left
                new Vector2Int(1, 0) // Right
            };

        foreach (var direction in directions)
        {
            Vector2Int check = new Vector2Int(node.GridX, node.GridY) + direction;

            if (check.x >= 0 && check.x < _x && check.y >= 0 && check.y < _y)
            {
                neighbors.Add(_tiles[check.x, check.y].Node);
            }
        }

        return neighbors;
    }

    public Node GetNode(Vector2Int startPos)
    {
        return _tiles[startPos.x, startPos.y].Node;
    }
}