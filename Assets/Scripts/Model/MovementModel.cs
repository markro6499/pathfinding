using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class MovementModel
{
    public List<Vector2Int> Path { get; private set; }

    public MovementModel(List<Vector2Int> path)
    {
        Path = path;
    }
}
