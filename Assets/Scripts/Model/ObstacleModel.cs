using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleModel
{
    private int _id = -1;
    public int Id => _id;

    private Vector2Int _position;
    public Vector2Int Position => _position;

    public ObstacleModel(int id, Vector2Int position)
    {
        _id = id;
        _position = position;
    }
}
