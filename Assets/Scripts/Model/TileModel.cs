using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class TileModel
{
    private int _x;
    private int _y;
    private Node _node;

    public int X => _x;
    public int Y => _y;
    public Node Node => _node;
    

    public int OccupantId { get; private set; }
    public bool IsOccupied { get { return OccupantId >= 0; } }

    public TileModel(int x, int y, Node node) 
    {
        _x = x;
        _y = y;
        _node = node;
        OccupantId = -1;
    }

    public void Clear()
    {
        OccupantId = -1;
        _node.Walkable = true;
    }

    public void SetOccupant(int occupantId)
    {
        OccupantId = occupantId;
        _node.Walkable = false;
    }
}
