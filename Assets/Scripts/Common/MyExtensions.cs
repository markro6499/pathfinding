﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
using System.Globalization;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEditor;

public static class MyExtensions
{
    public static bool ContainsAllItems<T>(this IEnumerable<T> a, IEnumerable<T> b)
    {
        return !b.Except(a).Any();
    }

    //----------------------------------------------------------------------------------------------------------------------

    public static string GetDescription<T>(this T e) where T : IConvertible
    {
        if (e is Enum)
        {
            Type type = e.GetType();
            Array values = Enum.GetValues(type);

            foreach (int val in values)
            {
                if (val == e.ToInt32(CultureInfo.InvariantCulture))
                {
                    var memInfo = type.GetMember(type.GetEnumName(val));
                    var descriptionAttribute = memInfo[0]
                        .GetCustomAttributes(typeof(DescriptionAttribute), false)
                        .FirstOrDefault() as DescriptionAttribute;

                    if (descriptionAttribute != null)
                    {
                        return descriptionAttribute.Description;
                    }
                }
            }
        }

        return string.Empty;
    }

    //----------------------------------------------------------------------------------------------------------------------

    public static Dictionary<TKey, TValue> Shuffle<TKey, TValue>(this Dictionary<TKey, TValue> source)
    {
        System.Random r = new System.Random();
        return source.OrderBy(x => r.Next())
           .ToDictionary(item => item.Key, item => item.Value);
    }

    //----------------------------------------------------------------------------------------------------------------------

    public static Color ParseHtmlColor(this string htmlValue)
    {
        Color newCol;
        if (ColorUtility.TryParseHtmlString(htmlValue, out newCol))
        {
            return newCol;
        }
        else
            return Color.black;
    }

    //----------------------------------------------------------------------------------------------------------------------

    public static Vector2Int ToVector2Int(this Vector3 v3)
    {
        return new Vector2Int((int)v3.x, (int)v3.z);
    }

    //----------------------------------------------------------------------------------------------------------------------

    public static Vector2Int ToVector2Int(this Vector3Int v3Int)
    {
        return new Vector2Int(v3Int.x, v3Int.z);
    }

    //----------------------------------------------------------------------------------------------------------------------

    public static Vector3 ToVector3(this Vector2Int v2Int)
    {
        return new Vector3(v2Int.x, 0, v2Int.y);
    }

    //----------------------------------------------------------------------------------------------------------------------

    public static int ConvertToAngle(this Direction self)
    {
        return DIRECTION_ANGLES_DICT[self];
    }

    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public static Direction GetDirection(this Vector3 p1, Vector3 p2)
    {
        Vector2 v2source = new Vector2(p1.x, p1.z);
        Vector2 v2target = new Vector2(p2.x, p2.z);

        return v2source.GetDirection(v2target);
    }

    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public static Direction GetDirection(this Vector2 p1, Vector2 p2)
    {
        float angle = Mathf.Atan2(p2.y - p1.y, p2.x - p1.x);
        int octant = Convert.ToInt32(8 * angle / (2 * Mathf.PI) + 8) % 8;
        Direction direction = (Direction)octant;

        return direction;
    }

    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public readonly static Dictionary<Direction, int> DIRECTION_ANGLES_DICT = new Dictionary<Direction, int> {
        { Direction.NE, 45 },
        { Direction.N, 0 },
        { Direction.NW, 315 },
        { Direction.W, 270 },
        { Direction.SW, 225 },
        { Direction.S, 180 },
        { Direction.SE, 135 },
        { Direction.E, 90 }
    };

    //----------------------------------------------------------------------------------------------------------------------

    public static List<T> Shuffle<T>(this List<T> stack)
    {
        System.Random rnd = new System.Random();
        return new List<T>(stack.OrderBy(x => rnd.Next()));
    }

    //----------------------------------------------------------------------------------------------------------------------

    public static Stack<T> Shuffle<T>(this Stack<T> stack)
    {
        System.Random rnd = new System.Random();
        return new Stack<T>(stack.OrderBy(x => rnd.Next()));
    }

    //----------------------------------------------------------------------------------------------------------------------

    public static Queue<T> Shuffle<T>(this Queue<T> queue)
    {
        System.Random rnd = new System.Random();
        return new Queue<T>(queue.OrderBy(x => rnd.Next()));
    }

    //----------------------------------------------------------------------------------------------------------------------

    public static TEnum ToEnum<TEnum>(this string strEnumValue, TEnum defaultValue)
    {
        if (!Enum.IsDefined(typeof(TEnum), strEnumValue))
            return defaultValue;

        return (TEnum)Enum.Parse(typeof(TEnum), strEnumValue);
    }

    //----------------------------------------------------------------------------------------------------------------------

    public static bool EnumTryParse(Type type, string input, out object value)
    {
        if (type == null)
            throw new ArgumentNullException("type");

        if (!type.IsEnum)
            throw new ArgumentException(null, "type");

        if (input == null)
        {
            value = Activator.CreateInstance(type);
            return false;
        }

        input = input.Trim();
        if (input.Length == 0)
        {
            value = Activator.CreateInstance(type);
            return false;
        }

        string[] names = Enum.GetNames(type);
        if (names.Length == 0)
        {
            value = Activator.CreateInstance(type);
            return false;
        }

        Type underlyingType = Enum.GetUnderlyingType(type);
        Array values = Enum.GetValues(type);
        // some enums like System.CodeDom.MemberAttributes *are* flags but are not declared with Flags...
        if ((!type.IsDefined(typeof(FlagsAttribute), true)) && (input.IndexOfAny(_enumSeperators) < 0))
            return EnumToObject(type, underlyingType, names, values, input, out value);

        // multi value enum
        string[] tokens = input.Split(_enumSeperators, StringSplitOptions.RemoveEmptyEntries);
        if (tokens.Length == 0)
        {
            value = Activator.CreateInstance(type);
            return false;
        }

        ulong ul = 0;
        foreach (string tok in tokens)
        {
            string token = tok.Trim(); // NOTE: we don't consider empty tokens as errors
            if (token.Length == 0)
                continue;

            object tokenValue;
            if (!EnumToObject(type, underlyingType, names, values, token, out tokenValue))
            {
                value = Activator.CreateInstance(type);
                return false;
            }

            ulong tokenUl;
            switch (Convert.GetTypeCode(tokenValue))
            {
                case TypeCode.Int16:
                case TypeCode.Int32:
                case TypeCode.Int64:
                case TypeCode.SByte:
                    tokenUl = (ulong)Convert.ToInt64(tokenValue, CultureInfo.InvariantCulture);
                    break;

                //case TypeCode.Byte:
                //case TypeCode.UInt16:
                //case TypeCode.UInt32:
                //case TypeCode.UInt64:
                default:
                    tokenUl = Convert.ToUInt64(tokenValue, CultureInfo.InvariantCulture);
                    break;
            }

            ul |= tokenUl;
        }
        value = Enum.ToObject(type, ul);
        return true;
    }

    //----------------------------------------------------------------------------------------------------------------------

    private static char[] _enumSeperators = new char[] { ',', ';', '+', '|', ' ' };

    //----------------------------------------------------------------------------------------------------------------------

    private static object EnumToObject(Type underlyingType, string input)
    {
        if (underlyingType == typeof(int))
        {
            int s;
            if (int.TryParse(input, out s))
                return s;
        }

        if (underlyingType == typeof(uint))
        {
            uint s;
            if (uint.TryParse(input, out s))
                return s;
        }

        if (underlyingType == typeof(ulong))
        {
            ulong s;
            if (ulong.TryParse(input, out s))
                return s;
        }

        if (underlyingType == typeof(long))
        {
            long s;
            if (long.TryParse(input, out s))
                return s;
        }

        if (underlyingType == typeof(short))
        {
            short s;
            if (short.TryParse(input, out s))
                return s;
        }

        if (underlyingType == typeof(ushort))
        {
            ushort s;
            if (ushort.TryParse(input, out s))
                return s;
        }

        if (underlyingType == typeof(byte))
        {
            byte s;
            if (byte.TryParse(input, out s))
                return s;
        }

        if (underlyingType == typeof(sbyte))
        {
            sbyte s;
            if (sbyte.TryParse(input, out s))
                return s;
        }

        return null;
    }

    //----------------------------------------------------------------------------------------------------------------------

    private static bool EnumToObject(Type type, Type underlyingType, string[] names, Array values, string input, out object value)
    {
        for (int i = 0; i < names.Length; i++)
        {
            if (string.Compare(names[i], input, StringComparison.OrdinalIgnoreCase) == 0)
            {
                value = values.GetValue(i);
                return true;
            }
        }

        if ((char.IsDigit(input[0]) || (input[0] == '-')) || (input[0] == '+'))
        {
            object obj = EnumToObject(underlyingType, input);
            if (obj == null)
            {
                value = Activator.CreateInstance(type);
                return false;
            }
            value = obj;
            return true;
        }

        value = Activator.CreateInstance(type);
        return false;
    }

    //----------------------------------------------------------------------------------------------------------------------

    public static List<List<T>> BreakIntoChunks<T>(List<T> list, int chunkSize)
    {
        if (chunkSize <= 0)
        {
            throw new ArgumentException("chunkSize must be greater than 0.");
        }

        List<List<T>> retVal = new List<List<T>>();

        while (list.Count > 0)
        {
            int count = list.Count > chunkSize ? chunkSize : list.Count;
            retVal.Add(list.GetRange(0, count));
            list.RemoveRange(0, count);
        }

        return retVal;
    }

    //----------------------------------------------------------------------------------------------------------------------

    public static IEnumerable<IList<T>> Permute<T>(this IList<T> list, int length)
    {
        if (list == null || list.Count == 0 || length <= 0)
        {
            yield break;
        }

        if (length > list.Count)
        {
            throw new ArgumentOutOfRangeException("length",
                                                  "length must be between 1 and the length of the list inclusive");
        }

        for (int i = 0; i < list.Count; i++)
        {
            var item = list[i];
            var initial = new[] { item };
            if (length == 1)
            {
                yield return initial;
            }
            else
            {
                foreach (var variation in Permute(list.Where((x, index) => index != i).ToList(), length - 1))
                {
                    yield return initial.Concat(variation).ToList();
                }
            }
        }
    }

    //----------------------------------------------------------------------------------------------------------------------

    public static IEnumerable<IEnumerable<T>> GetPermutations<T>(IEnumerable<T> list, int length)
    {
        if (length == 1) return list.Select(t => new T[] { t });

        return GetPermutations(list, length - 1)
            .SelectMany(t => list.Where(e => !t.Contains(e)),
                (t1, t2) => t1.Concat(new T[] { t2 }));
    }

    //----------------------------------------------------------------------------------------------------------------------

    public static T Clone<T>(this T source)
    {
        if (!typeof(T).IsSerializable)
        {
            throw new ArgumentException("The type must be serializable.", "source");
        }

        // Don't serialize a null object, simply return the default for that object
        if (ReferenceEquals(source, null))
        {
            return default(T);
        }

        IFormatter formatter = new BinaryFormatter();
        Stream stream = new MemoryStream();
        using (stream)
        {
            formatter.Serialize(stream, source);
            stream.Seek(0, SeekOrigin.Begin);
            return (T)formatter.Deserialize(stream);
        }
    }

    //----------------------------------------------------------------------------------------------------------------------

    public static Transform DestroyAllChildrens(this Transform transform)
    {
        foreach (Transform child in transform)
        {
            GameObject.Destroy(child.gameObject);
        }
        return transform;
    }

    //----------------------------------------------------------------------------------------------------------------------

    public static void SwapValues<T>(this T[] source, long index1, long index2)
    {
        T temp = source[index1];
        source[index1] = source[index2];
        source[index2] = temp;
    }

    //----------------------------------------------------------------------------------------------------------------------

    public static T[,] To2DArray<T>(IList<T[]> source)
    {
        int minorLength = source[0].Length;
        T[,] ret = new T[source.Count, minorLength];
        for (int i = 0; i < source.Count; i++)
        {
            var array = source[i];
            if (array.Length != minorLength)
            {
                throw new ArgumentException
                    ("All arrays must be the same length");
            }
            for (int j = 0; j < minorLength; j++)
            {
                ret[i, j] = array[j];
            }
        }
        return ret;
    }

    //----------------------------------------------------------------------------------------------------------------------

    public static int FindNearestValueIndex(this IList<float> self, float targetValue)
    {
        int closestValueIndex = 0;
        float minDifference = Mathf.Abs(self[0] - targetValue);

        for (int i = 1; i < self.Count; i++)
        {
            float difference = Mathf.Abs(self[i] - targetValue);

            if (difference < minDifference)
            {
                minDifference = difference;
                closestValueIndex = i;
            }
        }

        return closestValueIndex;
    }

    //----------------------------------------------------------------------------------------------------------------------
    public static void FillArray<T>(this T[,] self, T value)
    {
        for (int i = 0; i < self.GetLength(0); i++)
        {
            for (int j = 0; j < self.GetLength(1); j++)
            {
                self[i, j] = value;
            }
        }
    }

    //----------------------------------------------------------------------------------------------------------------------

    public static void Clear<T>(this T[] self)
    {
        Array.Clear(self, 0, self.Length);
    }

    //----------------------------------------------------------------------------------------------------------------------

    public static void Clear<T>(this T[,] self)
    {
        Array.Clear(self, 0, self.Length);
    }

    //----------------------------------------------------------------------------------------------------------------------
#if UNITY_EDITOR
    public static List<T> FindAssetsByType<T>() where T : UnityEngine.Object
    {
	    List<T> assets = new List<T>();

	    string[] guids = AssetDatabase.FindAssets(string.Format("t:{0}", typeof(T)));

	    for( int i = 0; i < guids.Length; i++ )
	    {
		    string assetPath = AssetDatabase.GUIDToAssetPath(guids[i]);
  	        T asset = AssetDatabase.LoadAssetAtPath<T>( assetPath );
  	        if( asset != null )
  	        {
  		        assets.Add(asset);
  	        }
        }
        return assets;
    }
#endif
    //----------------------------------------------------------------------------------------------------------------------

}



