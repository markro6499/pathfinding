using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum Direction
{
    E = 0, NE = 1,
    N = 2, NW = 3,
    W = 4, SW = 5,
    S = 6, SE = 7,
}

public enum GameMode
{
    PlayMode,
    EditMode,
}