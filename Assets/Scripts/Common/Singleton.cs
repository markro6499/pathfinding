using UnityEngine;

public abstract class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
    protected static T _instance;

    public static bool HasInstance => _instance != null;

    public static T Instance { get { return _instance; } }


    protected virtual void Awake()
    {
        if (HasInstance && _instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            _instance = this as T;
        }
    }
}