using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/MapTemplate", order = 1)]
public class MapTemplate : ScriptableObject
{
    [SerializeField] private string displayName;
    [SerializeField] private string description;
    [SerializeField] private Vector2Int startPosition;
    [SerializeField] private Vector2Int mapSize;

    [SerializeField] private Vector2Int[] obstaclesPositions;

    public string DisplayName => displayName;
    public string Description => description;
    public Vector2Int StartPosition => startPosition;
    public Vector2Int MapSize => mapSize;
    public Vector2Int[] ObstaclesPositions => obstaclesPositions;
}