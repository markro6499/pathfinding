using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Selector : MonoBehaviour
{
    [SerializeField] private MeshRenderer meshRenderer;
    public void Show(Vector2Int position)
    {
        meshRenderer.enabled = true;
        Vector3 offset = new Vector3(0.5f, 0.05f, 0.5f);
        transform.position = position.ToVector3() + offset;
    }

    public void Hide() 
    {
        meshRenderer.enabled = false;
    }
}
