using System;
using System.Collections;
using UnityEngine;

public class CharacterView : MonoBehaviour
{
    [SerializeField] private Transform fixTransform;
    [SerializeField] private Animator animator;
    [SerializeField] private Vector3 viewOffset = new Vector3(0.5f, 0.0f, 0.5f);

    [SerializeField] private string animationParameterName = "Speed";
    [SerializeField] private float animationSpeed = 1.0f;
    [SerializeField] private float tileMoveDuration = 1.0f;

    public bool IsMoving { get; private set; }

    private void Awake()
    {
        //animator.Play();
    }

    public void Setup(Vector3 startPosition)
    {
        transform.position = startPosition + viewOffset;
    }

    public void MoveToPosition(MovementModel movementData, Action onFinishCallback)
    {
        StartCoroutine(MovingCoroutine(movementData, onFinishCallback));

        //transform.position = destination + new Vector3(0.5f, 0, 0.5f);//;new Vector3(destination.x, 0, destination.y);
    }

    public void FaceTarget(Vector3 source, Vector3 target)
    {
        Direction direction = source.GetDirection(target);
        Rotate(direction);
    }

    public void Rotate(Direction direction)
    {
        fixTransform.rotation = Quaternion.Euler(Vector3.up * direction.ConvertToAngle());
    }

    private IEnumerator MovingCoroutine(MovementModel movementData, Action onFinishCallback)
    {
        IsMoving = true;

        animator.SetFloat(animationParameterName, animationSpeed);

        Vector3 sizeOffset = Vector3.zero;// GetObjectOffset();

        Vector2Int startPos = movementData.Path[0];
        Vector2Int nextPos = movementData.Path[1];

        float totalElapsedTime = 0;

        float totalMovementDuration = (movementData.Path.Count - 1) * tileMoveDuration;

        for (int i = 0; i < movementData.Path.Count - 1; i++)
        {
            float elapsedTime = 0;
            float progress = elapsedTime / tileMoveDuration;

            startPos = movementData.Path[i];
            nextPos = movementData.Path[i + 1];

            FaceTarget(startPos.ToVector3(), nextPos.ToVector3());

            while (progress < 1f)
            {
                elapsedTime += Time.deltaTime;
                totalElapsedTime += Time.deltaTime;
                progress = elapsedTime / tileMoveDuration;

                Vector3 newPosition = Vector3.Lerp(startPos.ToVector3(), nextPos.ToVector3(), progress);
                //float t = i == 0 || i == path.Count - 1 ? _curve.Evaluate(ratio) : ratio;

                newPosition.y = 0;

                transform.position = newPosition + viewOffset;

                yield return null;
            }

            transform.position = nextPos.ToVector3() + viewOffset;
        }

        animator.SetFloat(animationParameterName, 0.0f);

        onFinishCallback?.Invoke();

        IsMoving = false;
    }
}
