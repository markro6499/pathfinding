using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class DynamicLineRenderer : MonoBehaviour
{
    public Material lineMaterial;
    public float lineWidth = 0.1f;
    private Vector3[] linePoints = new Vector3[0];

    public void SetPoints(Vector3[] points)
    {
        linePoints = points;
    }

    public void ClearPoints()
    {
        Vector3[] t = new Vector3[0];
        SetPoints(t);
    }

    private void OnRenderObject()
    {
        if (linePoints.Length == 0)
            return;

        GL.PushMatrix();
        lineMaterial.SetPass(0);

        GL.Begin(GL.QUADS);

        for (int i = 0; i < linePoints.Length - 1; i++)
        {
            Vector3 startPoint = linePoints[i];
            Vector3 endPoint = linePoints[i + 1];
            Vector3 lineDirection = (endPoint - startPoint).normalized;
            Vector3 lineNormal = Vector3.Cross(lineDirection, Vector3.up).normalized;

            //GL.Color(lineMaterial.color);
            GL.TexCoord2(0, 0);
            GL.Vertex(startPoint - lineNormal * lineWidth / 2);
            GL.TexCoord2(0, 1);
            GL.Vertex(startPoint + lineNormal * lineWidth / 2);
            GL.TexCoord2(1, 1);
            GL.Vertex(endPoint + lineNormal * lineWidth / 2);
            GL.TexCoord2(1, 0);
            GL.Vertex(endPoint - lineNormal * lineWidth / 2);
            
        }

        GL.End();
        GL.PopMatrix();
    }

    void Start()
    {
    }
}