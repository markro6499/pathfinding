using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleView : MonoBehaviour
{
    private int _id = -1;
    public int Id => _id;

    [SerializeField] private Vector3 viewOffset = new Vector3(0.5f, 0.0f, 0.5f);

    public void Setup(int id, Vector3 position)
    {
        _id = id;
        transform.position = position + viewOffset;
    }
}
