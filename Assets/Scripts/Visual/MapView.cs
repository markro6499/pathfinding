using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapView : MonoBehaviour
{
    private List<ObstacleView> _obstaclesViews = new List<ObstacleView>();

    public void Setup(List<ObstacleView> obstaclesViews)
    {
        _obstaclesViews = obstaclesViews;
    }
    
    public void AddObstacle(ObstacleView obstacleView)
    {
        _obstaclesViews.Add(obstacleView);
    }

    public void RemoveObstacle(int id)
    {
        for (int i = _obstaclesViews.Count; i-- > 0;)
        {
            if (_obstaclesViews[i].Id == id)
            {
                Destroy(_obstaclesViews[i].gameObject);
                return;
            }
        }
    }

    public void RemoveAllObstacles()
    {
        for (int i= _obstaclesViews.Count; i-- > 0; )
        {
            Destroy(_obstaclesViews[i].gameObject);
        }

        _obstaclesViews = null;
    }


}
