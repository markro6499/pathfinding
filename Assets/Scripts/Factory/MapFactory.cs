using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapFactory : MonoBehaviour
{
    [SerializeField] private Material material;
    [SerializeField] private float fieldSize = 1;
    [SerializeField] private float textureScaleX = 1;
    [SerializeField] private float textureScaleY = 1;

    [SerializeField] private Renderer colorsMaterial;

    [SerializeField] private ObstacleView obstacleViewPrefab;
    [SerializeField] private MapView mapView;

    public MapView MapView => mapView;

    public MapView CreateMapView(MapModel mapModel)
    {
        int width = mapModel.X;
        int height = mapModel.Y;

        colorsMaterial.material.SetVectorArray("colorArray", new List<Vector4>(1) { new Vector4(1f, 1f, 1f, 1f) });

        GameObject quad = GameObject.CreatePrimitive(PrimitiveType.Quad);
        quad.name = "Default";
        quad.transform.parent = transform;
        quad.transform.localRotation = Quaternion.identity;
        quad.transform.Rotate(90, 0, 0);
        quad.transform.localScale = new Vector3(fieldSize * width, fieldSize * height, 1);
        quad.transform.localPosition = new Vector3(width * .5f, 0, height * .5f);

        MeshRenderer meshRenderer = quad.GetComponent<MeshRenderer>();
        meshRenderer.material = material;
        material.mainTextureScale = new Vector2(1 * width / textureScaleX, 1 * height / textureScaleY);
        /*
        var filter = GetComponent<MeshFilter>();
        Vector3 normal = Vector3.up;


        if (filter && filter.mesh.normals.Length > 0)
            normal = filter.transform.TransformDirection(filter.mesh.normals[0]);

        var plane = new Plane(normal, transform.position);
        */

        //TODO Move to ObstacleFactory
        for (int i = 0; i < mapModel.Obstacles.Count; i++) 
        {
            AddObstacle(mapModel.Obstacles[i].Id, mapModel.Obstacles[i].Position);
        }

        return mapView;
    }

    public void AddObstacle(int id, Vector2Int position)
    {
        ObstacleView obstacleView = Instantiate(obstacleViewPrefab);
        obstacleView.Setup(id, position.ToVector3());
        mapView.AddObstacle(obstacleView);
    }

    internal void RemoveObstacle(int id)
    {
        mapView.RemoveObstacle(id);
    }
}
