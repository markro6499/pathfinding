using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapManager : MonoBehaviour
{
    [SerializeField] private MapFactory factory;
    private MapModel _mapModel;

    [SerializeField] private MapTemplate defaultMapTemplate;
    public MapModel MapModel => _mapModel;
    public MapView MapView => factory.MapView;

    public void CreateMap()
    {
        CreateMap(defaultMapTemplate);
    }

    public void CreateMap(MapTemplate template)
    {
        _mapModel = new MapModel(template);
        factory.CreateMapView(_mapModel);
    }

    public void AddObstacle(Vector2Int destination)
    {
        ObstacleModel model = _mapModel.AddObstacle(destination);
        factory.AddObstacle(model.Id, model.Position);
    }

    public void RemoveObstacle(int id)
    {
        _mapModel.RemoveObstacle(id);
        factory.RemoveObstacle(id);
    }
}
