using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using static CameraControlActions;

public class CameraManager : Singleton<CameraManager>
{
    [SerializeField] private CameraController defaultCameraController;

    public void Setup(Vector3 startPosition)
    {
        transform.position = startPosition;
        defaultCameraController.Setup();
    }
}
