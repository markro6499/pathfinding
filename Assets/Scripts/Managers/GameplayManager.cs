using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class GameplayManager : MonoBehaviour
{
    [SerializeField] private MapManager mapManager;
    [SerializeField] private CharacterView characterViewPrefab;
    [SerializeField] private DynamicLineRenderer pathDotRenderer;
    [SerializeField] private Selector selector;

    [SerializeField] private Color color1;
    [SerializeField] private Color color2;

    private CharacterView _characterView;
    private CharacterModel _characterModel;

    private GameMode _gameMode;

    private void Start()
    {
        mapManager.CreateMap();

        Vector3 startWorldPositon = Vector3.zero;
        Vector2Int startMapPosition = Vector2Int.zero;

        if (mapManager.MapModel.X < 1)
        {
            Debug.LogError("X <= 0"); //TODO Error handling
        }
        else if (mapManager.MapModel.Y < 1)
        {
            Debug.LogError("Y <= 0");
        }

        if (mapManager.MapModel.X > mapManager.MapModel.Config.StartPosition.x && mapManager.MapModel.X > mapManager.MapModel.Config.StartPosition.x)
        {
            startWorldPositon = new Vector3(mapManager.MapModel.Config.StartPosition.x, 0.0f, mapManager.MapModel.Config.StartPosition.y);
            startMapPosition = mapManager.MapModel.Config.StartPosition;
        }
        

        _characterModel = new CharacterModel(startMapPosition);
        //TODO Move to CharacterFactory
        _characterView = Instantiate(characterViewPrefab);
        _characterView.Setup(startWorldPositon);

        CameraManager.Instance.Setup(startWorldPositon);

        _gameMode = GameMode.PlayMode;
    }

    private void Update()
    {
        Vector2Int selectedPosition = InputManager.Instance.GetMouseWorldPositionInt().ToVector2Int();

        if (IsNotCharacterPosition(selectedPosition) == true && IsInsideBounds(selectedPosition))
        {
            selector.Show(selectedPosition);
        }
        else
        {
            selector.Hide();
        }

        //TODO Remove Update and take it to EventManager
        if (_gameMode == GameMode.PlayMode)
        {
            CheckForCharacterMove(selectedPosition);
        }
        else if (_gameMode == GameMode.EditMode)
        {
            CheckForEditObstacleAction(selectedPosition);
        }   
    }

    private void CheckForCharacterMove(Vector2Int destination)
    {
        if (Mouse.current.leftButton.wasReleasedThisFrame && _characterView.IsMoving == false)
        {
            if (IsNotCharacterPosition(destination) == true && IsInsideBounds(destination))
            {
                List<Vector2Int> path = Pathfinding.FindPath(_characterModel.CurrentPosition, destination, mapManager.MapModel);
                MovementModel movementData = new MovementModel(path);

                MoveCharacter(movementData);

                ShowPath(path);
            }
        }
    }

    private void CheckForEditObstacleAction(Vector2Int destination)
    {
        if (Mouse.current.leftButton.wasReleasedThisFrame)
        {
            if (IsNotCharacterPosition(destination) == true && IsInsideBounds(destination))
            {
                if (mapManager.MapModel.Tiles[destination.x, destination.y].IsOccupied == true)
                {
                    mapManager.RemoveObstacle(mapManager.MapModel.Tiles[destination.x, destination.y].OccupantId);
                }
                else
                {
                    mapManager.AddObstacle(destination);
                }
            }
        }
    }

    private bool IsNotCharacterPosition(Vector2Int destination)
    {
        return destination.x != _characterModel.CurrentPosition.x || destination.y != _characterModel.CurrentPosition.y;
    }

    private bool IsInsideBounds(Vector2Int destination)
    {
        return destination.x >= 0 && destination.y >= 0 && destination.x < mapManager.MapModel.X && destination.y < mapManager.MapModel.Y;
    }

    private void MoveCharacter(MovementModel movementData)
    {
        if (movementData.Path.Count == 0)
        {
            //Can't move into position. There is no path.
            return;
        }

        Vector2Int destination = movementData.Path[movementData.Path.Count - 1];
        _characterModel.MoveToPosition(destination);
        _characterView.MoveToPosition(movementData, OnMoveAnimationFinish);
    }

    private void OnMoveAnimationFinish()
    {
        //On animation finish
    }

    public void ShowPath(List<Vector2Int> path)
    {
        Vector3 offset = new Vector3(0.5f, 0, 0.5f);
        Vector3[] vector3s = new Vector3[path.Count];

        for (int i = 0; i < path.Count; i++)
        {
            vector3s[i] = path[i].ToVector3() + Vector3.up * 0.01f + offset;
        }

        pathDotRenderer.SetPoints(vector3s);
    }

    public void HighlightArea()
    {

    }

    public void ToggleGameMode(bool val)
    {
        //TODO Every change of state from UI should be done by EventManager.
        _gameMode = val ? GameMode.EditMode : GameMode.PlayMode;

        Debug.Log(_gameMode);
    }
}
