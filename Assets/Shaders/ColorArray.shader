Shader "Custom/ColorArray"
{

    SubShader {
        Pass {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

			float4 colorArray[10];

            struct v2f {
                float4 vertex : SV_POSITION;
                float4 color : COLOR;
            };

            v2f vert (float4 vertex : POSITION, uint id : SV_VertexID) {
                v2f o;
                o.vertex = UnityObjectToClipPos(vertex);
                o.color = colorArray[id % 10];
                return o;
            }

            fixed4 frag (v2f i) : SV_Target {
                return i.color;
            }
            ENDCG
        }
    }
}
